import React from 'react'

const Weather = (props) => {
    return (
    <div className="ct">
        {props.country && props.city && <p>Location: {props.city},  {props.country}</p>}
        {props.temperature && <p>Temperature: {props.temperature}<span>&#8451;</span></p>}
        {props.humidity && <p>Humidity: {props.humidity} %</p>}
        {props.description && <p>Conditions:  {props.description}</p>}
        {props.latitude && props.longitude && <p>Geo Coords:  [{props.latitude}, {props.longitude}]</p>}
        {props.error && <p>{props.error}</p>}
    </div>
    )
}

export default Weather;