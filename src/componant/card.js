import React from 'react';

function Card(props) {

    return (
        <div>
            <div>
                <strong><h3 style={{ color: "#010101" }}>{props.city},India</h3></strong>
            </div>
            <div>
                <span>Temperature: {props.temperature}&#8451;</span>
            </div>
            <div>
                <span>Humidity: {props.humidity}%</span>
            </div>
            <div>
                <span>Conditions: {props.description}</span>
            </div>
            <div>
                <span>Geo Coords: [{props.latitude},{props.longitude}]</span>
            </div>
            <div>
                <span>{props.error}</span>
            </div></div>
    )
}

export default Card