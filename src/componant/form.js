import React, { Component } from 'react';
export class Form extends Component {
  render() {
    
    return (
      <div className="form">
      <form className="inp" onSubmit={this.props.loadWeather}>
        <input className="inpt" id="i1" type="text" name="city" placeholder="City" />
        <input className="inpt" id="i2" type="text" name="country" placeholder="Country" />
        <button className="inpt" id="bt1" ><span>&#10230;</span></button>
      </form>
      </div>
    )
  }
}
export default Form
