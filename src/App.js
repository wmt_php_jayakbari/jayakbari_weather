import React from 'react';
import Title from './componant/title';
import Form from './componant/form';
import Weather from './componant/weather';
import Card from './componant/card';
import axios from 'axios';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';


const Api_Key = "ed1eace09cb5eea60dc97f0c5677f931";
const kelvinToCelsius = require('kelvin-to-celsius');
class App extends React.Component {

  state = {
    rajkot: {},
    ahmedabad: {},
    surat: {},
    mumbai: {},
    pune: {},
    agra: {}
  }

  getWeather = async (e) => {
    try {
    const city = e.target.elements.city.value;
    const country = e.target.elements.country.value;
    e.preventDefault();
    const api_call = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${Api_Key}`);
    const response = await api_call.json();
    console.log(response);
    if (city && country) {
      this.setState({
        temperature: kelvinToCelsius(response.main.temp),
        city: response.name,
        country: response.sys.country,
        humidity: response.main.humidity,
        description: response.weather[0].description,
        longitude: response.coord.lon,
        latitude: response.coord.lat,
        error: ""
      })
    } else {
      this.setState({
        error: "Please, Enter the Values"
      })
    }
  }
  catch (e) {
    this.setState({
      error: "Please, Enter appropriate Data"})
  }
}

  componentDidMount() {
    {
      let cities = ['rajkot', 'ahmedabad', 'surat', 'mumbai', 'pune', 'agra'];
      var x = cities.map(city => {
        axios.get(`http://api.openweathermap.org/data/2.5/weather?q=${city},${"india"}&appid=${Api_Key}`).then((response) => {
          this.setState({
            [city]: {

              temperature: kelvinToCelsius(response.data.main.temp),
              city: response.data.name,
              country: response.data.sys.country,
              humidity: response.data.main.humidity,
              description: response.data.weather[0].description,
              longitude: response.data.coord.lon,
              latitude: response.data.coord.lat,
              error: ""
            }
          })
        })
      })
    }
  }

  render() {
    let cities = ['rajkot', 'ahmedabad', 'surat', 'mumbai', 'pune', 'agra'];
    const citi = cities.map((city) => {
      return (<div className="city">
        <Card temperature={this.state[city].temperature} city={this.state[city].city}
          country={this.state[city].country}
          humidity={this.state[city].humidity}
          description={this.state[city].description}
          longitude={this.state[city].longitude}
          latitude={this.state[city].latitude}
          error={this.state[city].error}
        /><br /></div>)
    })

    return (
      <div>
        <center>
          <div>
            <Title />
            <div className="disp">
            <Form loadWeather={this.getWeather} />
            <Weather
              temperature={this.state.temperature}
              city={this.state.city}
              country={this.state.country}
              humidity={this.state.humidity}
              description={this.state.description}
              longitude={this.state.longitude}
              latitude={this.state.latitude}
              error={this.state.error}
            />
</div>
          </div>
          <div className="demo">{citi}</div>
        </center>
      </div>

    )
  }

}
export default App;